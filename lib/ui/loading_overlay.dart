import 'dart:math';

import 'package:flutter/material.dart';

class LoadingOverlay extends StatefulWidget {
  final VoidCallback _onTap;

  LoadingOverlay(this._onTap);

  @override
  _LoadingOverlayOverlayState createState() => _LoadingOverlayOverlayState();
}

class _LoadingOverlayOverlayState extends State<LoadingOverlay>
    with SingleTickerProviderStateMixin {
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;

  @override
  void initState() {
    super.initState();

    _iconAnimationController =
        AnimationController(duration: Duration(milliseconds: 800), vsync: this);
    _iconAnimation = CurvedAnimation(
        parent: _iconAnimationController, curve: Curves.easeIn);
    _iconAnimationController.addListener(() {
      setState(() {});
    });
    _iconAnimationController.repeat();
  }

  @override
  void dispose() {
    _iconAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.blueAccent,
      child: InkWell(
        onTap: widget._onTap,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              decoration:
              BoxDecoration(color: Colors.white, shape: BoxShape.circle),
              child: Transform.rotate(
                angle: _iconAnimation.value * 2 * pi,
                child: Icon(
                  Icons.autorenew,
                  size: 80.0,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20.0),
            ),
            Text(
              'Loading...',
              style: TextStyle(color: Colors.white, fontSize: 30.0),
            )
          ],
        ),
      ),
    );
  }
}
