import 'dart:async';

import 'package:flutter/material.dart';

class QuestionText extends StatefulWidget {
  final String questionText;
  final int questionNumber;

  QuestionText(this.questionText, this.questionNumber);

  @override
  _QuestionTextState createState() => _QuestionTextState();
}

class _QuestionTextState extends State<QuestionText>
    with SingleTickerProviderStateMixin {
  Animation<double> _fontSizeAnimation;
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    _animationController =
        AnimationController(duration: Duration(milliseconds: 800), vsync: this);
    _fontSizeAnimation =
        CurvedAnimation(parent: _animationController, curve: Curves.elasticOut);

    _fontSizeAnimation.addListener(() => this.setState(() {}));
    Timer(Duration(milliseconds: 100), () {
      _animationController.forward();
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(QuestionText oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.questionText != widget.questionText) {
      _animationController.reset();
      _animationController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Center(
          child: Text(
            "Statement " +
                widget.questionNumber.toString() +
                ": " +
                widget.questionText,
            style: TextStyle(fontSize: _fontSizeAnimation.value * 15),
          ),
        ),
      ),
    );
  }
}
