import 'package:flutter/material.dart';

class AnswerButton extends StatefulWidget {
  final bool _answer;
  final VoidCallback _onTap;

  AnswerButton(this._answer, this._onTap);

  @override
  _AnswerButtonState createState() => _AnswerButtonState();
}

class _AnswerButtonState extends State<AnswerButton> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Material(
        color: widget._answer == true ? Colors.greenAccent : Colors.redAccent,
        child: InkWell(
          onTap: () => widget._onTap(),
          child: Center(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(width: 5.0, color: Colors.white)),
              padding: EdgeInsets.all(10.0),
              child: Text(
                widget._answer.toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 40.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
