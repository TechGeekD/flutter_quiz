import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_quiz/pages/score_page.dart';
import 'package:flutter_quiz/ui/answer_button.dart';
import 'package:flutter_quiz/ui/correct_wrong_overlay.dart';
import 'package:flutter_quiz/ui/loading_overlay.dart';
import 'package:flutter_quiz/ui/question_text.dart';
import 'package:flutter_quiz/utils/questions.dart';
import 'package:flutter_quiz/utils/quiz.dart';
import 'package:html_unescape/html_unescape.dart';
import 'package:http/http.dart' as http;

class QuestionMap {
  final int responseCode;
  final List<QuestionsList> questionsList;

  QuestionMap({this.responseCode, this.questionsList});

  factory QuestionMap.fromJson(Map<String, dynamic> json) {
    List list = json['results'] as List;
    List<QuestionsList> questionsList =
        list.map((i) => QuestionsList.fromJson(i)).toList();
    return QuestionMap(
        responseCode: json['response_code'], questionsList: questionsList);
  }
}

class QuestionsList {
  final String question;
  final bool correctAnswer;

  QuestionsList({this.question, this.correctAnswer});

  factory QuestionsList.fromJson(Map<String, dynamic> json) {
    var unescape = new HtmlUnescape();
    String question = unescape.convert(json['question']);
    return QuestionsList(
      question: question,
      correctAnswer: (json['correct_answer'] == 'True'),
    );
  }
}

class QuizPage extends StatefulWidget {
  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  Questions currentQuestion;
  Quiz quiz = Quiz([
    Questions('Q1', true),
  ]);

  String questionText;
  int questionNumber;
  bool isCorrect;
  bool overlayShouldBeVisible = false;
  bool isLoadingData = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      isLoadingData = true;
    });
    _getData().then((QuestionMap response) {
      List<Questions> questionList = response.questionsList
          .map((item) => Questions(item.question, item.correctAnswer))
          .toList();

      quiz = Quiz(questionList);
      setState(() {
        currentQuestion = quiz.nextQuestion;
        questionText = currentQuestion.questionName;
        questionNumber = quiz.questionNumber;
      });
      setState(() {
        isLoadingData = false;
      });
    }).catchError((err) {
      print('****************** $err *******************');
    });
  }

  Future<QuestionMap> _getData() async {
    Uri uri = Uri.https(
        'opentdb.com', '/api.php', {'amount': '10', 'type': 'boolean'});
    final http.Response response = await http.get(
      uri,
      headers: {
        'Accept': 'application/json',
      },
    );
    print(json.decode(response.body));
    return QuestionMap.fromJson(json.decode(response.body));
  }

  void handleAnswer(bool answer) {
    isCorrect = (currentQuestion.answer == answer);
    quiz.answer(isCorrect);
    this.setState(() {
      overlayShouldBeVisible = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        isLoadingData == false
            ? Column(
                children: <Widget>[
                  AnswerButton(true, () {
                    handleAnswer(true);
                  }),
                  QuestionText(questionText, questionNumber),
                  AnswerButton(false, () {
                    handleAnswer(false);
                  }),
                ],
              )
            : LoadingOverlay(() {
                this.setState(() {
                  overlayShouldBeVisible = false;
                });
              }),
        overlayShouldBeVisible == true
            ? CorrectWrongOverlay(isCorrect, () {
                if (quiz.length == questionNumber) {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              ScorePage(quiz.score, quiz.length)),
                      (Route route) => route == null);
                  return;
                }

                currentQuestion = quiz.nextQuestion;
                this.setState(() {
                  overlayShouldBeVisible = false;
                  questionText = currentQuestion.questionName;
                  questionNumber = quiz.questionNumber;
                });
              })
            : Container()
      ],
    );
  }
}
