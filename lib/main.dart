import 'package:flutter/material.dart';

import 'package:flutter_quiz/pages/landing_page.dart';

void main() {
  runApp(FlutterQuiz());
}

class FlutterQuiz extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LandingPage(),
    );
  }
}
